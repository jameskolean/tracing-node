const { Kafka } = require('kafkajs')
const instrumentKafkaJs = require('zipkin-instrumentation-kafkajs')
const { tracer } = require('../tracer')

const kafka = instrumentKafkaJs(
  new Kafka({
    clientId: 'todo-topic',
    brokers: ['localhost:9092'],
  }),
  {
    tracer, // Your zipkin tracer instance
    remoteServiceName: 'todo-Kafka', // This should be the symbolic name of the broker, not a consumer.
  }
)

const producer = kafka.producer()
const kafkaServer = {
  run: async () => {},
}

module.exports = { kafkaServer, producer }
