const Koa = require('koa')
const router = require('./routes/router')
const bodyParser = require('koa-body')
const { koaMiddleware } = require('zipkin-instrumentation-koa')
const { kafkaServer } = require('./messaging/kafka')
const { tracer, serviceName } = require('./tracer')

const app = new Koa()

app.use(bodyParser())
app.use(koaMiddleware({ tracer, serviceName }))
app.use(router())
kafkaServer.run().catch(console.error)
app.listen(3000)
