var Router = require('koa-router')
const { zipkinServiceFetch: fetch } = require('../tracer')
const url = 'http://localhost:3001/todos'
const { v4: uuidv4 } = require('uuid')
const { producer } = require('../messaging/kafka')

const router = Router({ prefix: '/todos' })

router.get('/', async (ctx) => {
  try {
    const response = await fetch(url)
    const json = await response.json()
    ctx.body = json
  } catch (error) {
    console.log(error)
    ctx.body = 'failure'
  }
})

router.post('/', async (ctx) => {
  const todoMessage = {
    description: ctx.request.body.description,
    transactionId: uuidv4(),
  }
  console.log('hello', todoMessage)
  await producer.send({
    topic: 'todo',
    messages: [{ value: JSON.stringify(todoMessage) }],
  })

  ctx.body = { success: true }
})

module.exports = router
