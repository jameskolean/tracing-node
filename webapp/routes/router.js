const TodoRouter = require('./todos')
const combineRouters = require('koa-combine-routers')

const router = combineRouters(TodoRouter)

module.exports = router
