# Tracing Example

This example uses Zipkin to traces REST and Kafka messages

## Layout of Example

### WebApp

| method | route  | event | action                                                                 |
| ------ | ------ | ----- | ---------------------------------------------------------------------- |
| GET    | /todos |       | calls service GET /todos                                               |
| POST   | /todos |       | publishes a create todo event in kafka {"description":"Remember This"} |

### service

| method | route  | event       | action                   |
| ------ | ------ | ----------- | ------------------------ |
| GET    | /todos |             | calls service GET /todos |
|        |        | Create todo | saves a todo             |

## Start Zipkin and Zookeeper/Kafka(messaging support)

`docker up`

## Endpoints

| service | URL                    |
| ------- | ---------------------- |
| Zipkin  | http://localhost:9411  |
| Kakfa   | http://localhost:9092 |
| WebApp  | http://localhost:3000  |
| Service | http://localhost:3001  |
