const { Kafka } = require('kafkajs')
const todoStore = require('../todoStore')
const instrumentKafkaJs = require('zipkin-instrumentation-kafkajs')
const { tracer } = require('../tracer')

const kafka = instrumentKafkaJs(
  new Kafka({
    clientId: 'todo-topic',
    brokers: ['localhost:9092'],
  }),
  {
    tracer, // Your zipkin tracer instance
    remoteServiceName: 'todo-kafka', // This should be the symbolic name of the broker, not a consumer.
  }
)

const consumer = kafka.consumer({ groupId: 'group_id' })
const kafkaServer = {
  run: async () => {
    // Consuming
    await consumer.connect()
    await consumer.subscribe({ topic: 'todo', fromBeginning: true })

    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        console.log({
          partition,
          offset: message.offset,
          value: message.value.toString(),
        })
        try {
          const payload = JSON.parse(message.value.toString())
          const newTodo = {
            description: payload.description,
            completed: false,
          }
          todoStore.addTodo(newTodo)
        } catch (error) {
          console.error('Unable to process messasge', message.value.toString())
        }
      },
    })
  },
}

module.exports = { kafkaServer }
