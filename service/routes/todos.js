var Router = require('koa-router')
const todoStore = require('../todoStore')

const router = Router({ prefix: '/todos' })

router.get('/', async (ctx) => {
  ctx.body = todoStore.getTodos()
})

module.exports = router
