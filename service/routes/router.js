const KoaRouter = require('koa-router')
const TodoRouter = require('./todos')
const combineRouters = require('koa-combine-routers')

const router = combineRouters(TodoRouter)

module.exports = router
