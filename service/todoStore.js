let todos = [
  { description: 'Get Milk', completed: false },
  { description: 'Get Bread', completed: false },
]
const todoStore = {
  getTodos: () => {
    return todos
  },
  addTodo: (todo) => {
    console.log('createTodos', todo)
    todos.push(todo)
  },
}

module.exports = todoStore
