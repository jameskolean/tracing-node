const {
  Tracer,
  BatchRecorder,
  jsonEncoder: { JSON_V2 },
} = require('zipkin')
const CLSContext = require('zipkin-context-cls')
const { HttpLogger } = require('zipkin-transport-http')
const serviceName = 'my-service'

const ctxImpl = new CLSContext('zipkin')
const recorder = new BatchRecorder({
  logger: new HttpLogger({
    endpoint: 'http://localhost:9411/api/v2/spans',
    jsonEncoder: JSON_V2,
  }),
})

const tracer = new Tracer({
  recorder,
  ctxImpl,
  localServiceName: serviceName,
})

module.exports = { tracer, serviceName }
